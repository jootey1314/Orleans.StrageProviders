﻿
using System.Security;
using MongoDB.Bson;
using MongoDB.Driver;
using Samples.StorageProviders;
using Orleans.Providers;
using System.Threading.Tasks;
using MongoDB.Driver.Core.Configuration;
using System;

namespace ActorMongo
{
    public abstract class MongoDbProvider: BaseJSONStorageProvider
    {       

        /// <summary>
        /// 获取collection集合对象
        /// </summary>
        protected readonly IMongoCollection<BsonDocument> _Collection;

        public abstract string MongoDb { get; }
        public abstract string CollecitonName { get; }
        private static string MongoUrl { get; set; }

        public string ConnectionString { get; set; }

        public string Database { get; set; }

        protected MongoDbProvider()
        {            
            string _db = MongoDb;
            if (string.IsNullOrEmpty(MongoUrl))
                MongoUrl = ConnectionString;// GetConnectionPath(actorService);//

            if (string.IsNullOrEmpty(MongoUrl))
                return;
            if (string.IsNullOrEmpty(_db))
                _db = "ServiceFabric";//设置默认值

            var db = new MongoClient(new MongoUrl(MongoUrl)).GetDatabase(_db);
            _Collection = db.GetCollection<BsonDocument>(CollecitonName);
        }


        public override Task Init(string name, IProviderRuntime providerRuntime, IProviderConfiguration config)
        {
            Name = name;
            ConnectionString = config.Properties["ConnectionString"];
            Database = config.Properties["Database"];
            if (string.IsNullOrWhiteSpace(ConnectionString)) throw new ArgumentException("ConnectionString property not set");
            if (string.IsNullOrWhiteSpace(Database)) throw new ArgumentException("Database property not set");
            DataManager = new GrainStateMongoDataManager(Database, ConnectionString);
            return base.Init(name, providerRuntime, config);
        }
  
    }

}
